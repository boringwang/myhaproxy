FROM index.alauda.cn/tutum/ubuntu:14.04
ENV ROOT_PASS="boringwang"
RUN apt-get install -y haproxy
ADD haproxy.cfg /etc/haproxy/haproxy.cfg
#RUN echo "        server server1 $IPPORT maxconn 20480" >> /etc/haproxy/haproxy.cfg
ADD start.sh /usr/local/bin/start.sh
RUN chmod 755 /usr/local/bin/start.sh
CMD ["sh", "-c", "start.sh"]
EXPOSE 172
